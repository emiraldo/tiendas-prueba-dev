from rest_framework import serializers

from .models import Venta, ProductoVenta


class VentaSerializer(serializers.ModelSerializer):
    """
        Serializador de datos para el modelo de Venta
    """
    class Meta:
        model = Venta
        fields = (
            'id',
            'cliente',
            'nombre_cliente',
            'nombre_empresa',
            'id_empresa',
            'total',
        )
        read_only_fields = ('nombre_cliente', 'nombre_empresa', 'id_empresa')


class ProductoVentaSerializer(serializers.ModelSerializer):
    """
        Serializador de datos para el modelo de ProductoVenta
    """
    class Meta:
        model = ProductoVenta
        fields = (
            'id',
            'venta',
            'producto',
            'nombre_producto',
            'valor_producto',
            'cantidad',
            'total',
        )
        read_only_fields = ('nombre_producto', 'valor_producto')
