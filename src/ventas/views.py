from rest_framework import viewsets, status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from .models import Venta, ProductoVenta
from .serializers import VentaSerializer, ProductoVentaSerializer


class VentaViewSet(viewsets.ModelViewSet):
    """
       retrieve:
       Retorna información de una venta dado su ID.

       list:
       Retorna el listado de ventas existentes.

       create:
       Servicio para la creación de una venta.

       update:
       Servicio para actualizar una venta (idempotente).

       partial_update:
       Servicio para actualizar una venta.

       delete:
       Servicio para eliminar una venta.
    """
    serializer_class = VentaSerializer
    queryset = Venta.objects.all()
    http_method_names = ('post', 'put', 'get', 'patch', 'delete')


class VentaListAPIView(ListAPIView):
    """
    Retorna el listado de ventas existentes a partir de una empresa.
    """
    queryset = []
    serializer_class = VentaSerializer
    lookup_url_kwarg = 'empresa'

    def get(self, request, *args, **kwargs):

        empresa = self.kwargs.get(self.lookup_url_kwarg)
        queryset = Venta.objects.filter(
            cliente__empresa__pk=empresa
        )
        ventas = []
        for venta in queryset:
            ventas.append({
                'id': venta.pk,
                'cliente': venta.cliente.pk,
                'nombre_cliente': venta.nombre_cliente(),
                'nombre_empresa': venta.nombre_empresa(),
                'total': venta.total,
            })

        return Response(ventas)


class ProductoVentaViewSet(viewsets.ModelViewSet):
    """
       retrieve:
       Retorna información de un producto de venta dado su ID.

       list:
       Retorna el listado de productos de ventas existentes.

       create:
       Servicio para la creación de un producto de venta.

       update:
       Servicio para actualizar un producto de venta (idempotente).

       partial_update:
       Servicio para actualizar un producto de venta.

       delete:
       Servicio para eliminar un producto de venta.
    """
    serializer_class = ProductoVentaSerializer
    queryset = ProductoVenta.objects.all()
    http_method_names = ('post', 'put', 'get', 'patch', 'delete')


class ProductoVentaListAPIView(ListAPIView):
    """
    Retorna el listado de productos de venta existentes a partir de una venta.
    """
    queryset = []
    serializer_class = ProductoVentaSerializer
    lookup_url_kwarg = 'venta'

    def get(self, request, *args, **kwargs):

        venta = self.kwargs.get(self.lookup_url_kwarg)
        queryset = ProductoVenta.objects.filter(
            venta__pk=venta
        )
        productos = []
        for producto in queryset:
            productos.append({
                'id': producto.pk,
                'venta': producto.venta.pk,
                'producto': producto.pk,
                'nombre_producto': producto.nombre_producto(),
                'valor_producto': producto.valor_producto(),
                'cantidad': producto.cantidad,
                'total': producto.total,
            })

        return Response(productos)
