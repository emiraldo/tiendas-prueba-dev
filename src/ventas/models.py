from django.db import models, transaction, DatabaseError

# Create your models here.
from django.http import Http404

from configuraciones.models import Auditoria
from empresas.models import Cliente
from productos.models import Producto


class Venta(Auditoria):
    """
        Modelo para almacenamiento de ventas de empresas
    """

    class Meta:
        verbose_name = 'Venta'
        verbose_name_plural = 'Ventas'

    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    total = models.PositiveIntegerField(default=0, blank=True, null=True)

    def nombre_cliente(self):
        if self.cliente:
            return u"%s" % self.cliente.nombres
        else:
            return u""

    nombre_cliente.short_description = 'Nombre cliente'
    nombre_cliente.allow_tags = False

    def nombre_empresa(self):
        if self.cliente:
            return u"%s" % self.cliente.nombre_empresa()
        else:
            return u""

    nombre_empresa.short_description = 'Nombre empresa'
    nombre_empresa.allow_tags = False

    def id_empresa(self):
        if self.cliente:
            return u"%d" % self.cliente.empresa.id
        else:
            return u""

    id_empresa.short_description = 'ID empresa'
    id_empresa.allow_tags = False

    def __str__(self):
        return "Venta de la empresa: " + str(self.cliente.empresa)


class ProductoVenta(Auditoria):
    """
        Modelo para almacenamiento de productos agregados a una venta de una empresa
    """

    class Meta:
        verbose_name = 'Producto de venta'
        verbose_name_plural = 'Productos de ventas'

    venta = models.ForeignKey(Venta, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.PositiveIntegerField(default=0)
    total = models.PositiveIntegerField(default=0, blank=True, null=True)

    def nombre_producto(self):
        if self.producto:
            return u"%s" % self.producto.nombre
        else:
            return u""

    nombre_producto.short_description = 'Nombre producto'
    nombre_producto.allow_tags = False

    def valor_producto(self):
        if self.producto:
            return self.producto.valor_venta
        else:
            return 0

    valor_producto.short_description = 'Valor producto'
    valor_producto.allow_tags = False

    def __str__(self):
        return 'Producto de venta de la empresa: ' + str(self.producto.empresa)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        '''
        Al guardar, se cacularan automaticamente el valor total
        '''
        try:
            with transaction.atomic():
                self.total = self.cantidad * self.producto.valor_venta
                self.full_clean()
                super(ProductoVenta, self).save(force_insert, force_update,
                                                 using,
                                                 update_fields)

                productos = ProductoVenta.objects.filter(venta=self.venta)
                self.venta.total = 0
                for producto in productos:
                    self.venta.total += producto.total
                self.venta.save()
        except DatabaseError:
            raise Http404

    def delete(self, *args, **kwargs):
        try:
            with transaction.atomic():
                self.venta.total -= self.total
                self.venta.save()
                super(ProductoVenta, self).delete(*args, **kwargs)
        except DatabaseError:
            raise Http404
