from django.urls import path, include
from django.conf.urls import url

from rest_framework.routers import SimpleRouter

from .views import VentaViewSet, ProductoVentaViewSet, VentaListAPIView, ProductoVentaListAPIView

router = SimpleRouter()

router.register(r'ventas', VentaViewSet)
router.register(r'productos_ventas', ProductoVentaViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('ventas_empresa/<int:empresa>/', VentaListAPIView.as_view(), name='ventas_list'),
    path('produtos_venta_venta/<int:venta>/', ProductoVentaListAPIView.as_view(), name='produtos_venta_list'),
]
