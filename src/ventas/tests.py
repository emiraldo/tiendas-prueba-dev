import random
from json import dumps, loads

from django.contrib.auth.models import User
from django.test import TestCase

from django_dynamic_fixture import G
from rest_framework import status

from configuraciones.utils import DefaultAPITest
from empresas.models import Empresa, Cliente
from productos.models import Producto
from ventas.models import Venta, ProductoVenta


class VentaViewSetTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_destroy(self):
        venta_test = G(Venta)

        url = '/api/ventas/{}/'.format(venta_test.id)
        response = self.client.delete(url)

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_destroy_unauthorized(self):
        venta_test = G(Venta)
        url = '/api/ventas/{}/'.format(venta_test.id)
        response = self.client_401.delete(url)

        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create(self):
        cliente_test = G(Cliente)
        data = {
            'cliente': cliente_test.id,
            'total': 1,
        }
        url = '/api/ventas/'
        response = self.client.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_create_unauthorized(self):
        cliente_test = G(Cliente)
        data = {
            'cliente': cliente_test.id,
            'total': 1,
        }
        url = '/api/ventas/'
        response = self.client_401.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_put(self):
        cliente_test = G(Cliente)
        venta_test = G(Venta, cliente=cliente_test)
        data = {
            'cliente': cliente_test.id,
            'total': 1,
        }
        url = '/api/ventas/{}/'.format(venta_test.id)
        response = self.client.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_put_unauthorized(self):
        cliente_test = G(Cliente)
        venta_test = G(Venta, cliente=cliente_test)
        data = {
            'cliente': cliente_test.id,
            'total': 1,
        }
        url = '/api/ventas/{}/'.format(venta_test.id)
        response = self.client_401.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_patch(self):
        cliente_test = G(Cliente)
        venta_test = G(Venta, cliente=cliente_test)
        data = {
            'total': 100
        }
        url = '/api/ventas/{}/'.format(venta_test.id)
        response = self.client.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_patch_unauthorized(self):
        cliente_test = G(Cliente)
        venta_test = G(Venta, cliente=cliente_test)
        data = {
            'total': 100
        }
        url = '/api/ventas/{}/'.format(venta_test.id)
        response = self.client_401.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list(self):
        x_max = random.randint(1, 10)
        cliente_test = G(Cliente)
        for x in range(1, x_max + 1, 1):
            G(Venta, cliente=cliente_test)
        url = '/api/ventas/'
        response = self.client.get(path=url, format='json')
        self.assertEquals(loads(dumps(response.data))['count'], x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        cliente_test = G(Cliente)
        for x in range(1, x_max + 1, 1):
            G(Venta, cliente=cliente_test)
        url = '/api/ventas/'
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)


class VentaListAPIViewTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_list(self):
        """
        Se genera un número aleatorio entre 1 y 10, se crea esa cantidad de Ventas pertenecientes a una empresa,
        de igual forma se crea la misma cantidad de Ventas sin ser asignados a esa empresa, el test debe arrojar
        la cantidad x_max de Ventas pertenecientes a la empresa.
        """
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        cliente_test = G(Cliente, empresa=empresa_test)
        for x in range(1, x_max + 1, 1):
            G(Venta, cliente=cliente_test)

        for x in range(1, x_max + 1, 1):
            G(Venta)
        url = '/api/ventas_empresa/{}/'.format(empresa_test.id)
        response = self.client.get(path=url, format='json')
        self.assertEquals(len(loads(dumps(response.data))), x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        cliente_test = G(Cliente, empresa=empresa_test)
        for x in range(1, x_max + 1, 1):
            G(Venta, cliente=cliente_test)

        for x in range(1, x_max + 1, 1):
            G(Venta)
        url = '/api/ventas_empresa/{}/'.format(empresa_test.id)
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ProductoVentaViewSetTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_destroy(self):
        producto_venta_test = G(ProductoVenta)

        url = '/api/productos_ventas/{}/'.format(producto_venta_test.id)
        response = self.client.delete(url)

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_destroy_unauthorized(self):
        producto_venta_test = G(ProductoVenta)

        url = '/api/productos_ventas/{}/'.format(producto_venta_test.id)
        response = self.client_401.delete(url)

        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create(self):
        venta_test = G(Venta)
        producto_test = G(Producto)
        data = {
            'venta': venta_test.id,
            'producto': producto_test.id,
            'cantidad': 1,
            'total': 1,
        }
        url = '/api/productos_ventas/'
        response = self.client.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_create_unauthorized(self):
        venta_test = G(Venta)
        producto_test = G(Producto)
        data = {
            'venta': venta_test.id,
            'producto': producto_test.id,
            'cantidad': 1,
            'total': 1,
        }
        url = '/api/productos_ventas/'
        response = self.client_401.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_put(self):
        venta_test = G(Venta)
        producto_test = G(Producto)
        producto_venta_test = G(ProductoVenta, venta=venta_test, producto=producto_test)
        data = {
            'venta': venta_test.id,
            'producto': producto_test.id,
            'cantidad': 1,
            'total': 1,
        }
        url = '/api/productos_ventas/{}/'.format(producto_venta_test.id)
        response = self.client.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_put_unauthorized(self):
        venta_test = G(Venta)
        producto_test = G(Producto)
        producto_venta_test = G(ProductoVenta, venta=venta_test, producto=producto_test)
        data = {
            'venta': venta_test.id,
            'producto': producto_test.id,
            'cantidad': 1,
            'total': 1,
        }
        url = '/api/productos_ventas/{}/'.format(producto_venta_test.id)
        response = self.client_401.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_patch(self):
        venta_test = G(Venta)
        producto_test = G(Producto)
        producto_venta_test = G(ProductoVenta, venta=venta_test, producto=producto_test)
        data = {
            'total': 1,
        }
        url = '/api/productos_ventas/{}/'.format(producto_venta_test.id)
        response = self.client.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_patch_unauthorized(self):
        venta_test = G(Venta)
        producto_test = G(Producto)
        producto_venta_test = G(ProductoVenta, venta=venta_test, producto=producto_test)
        data = {
            'total': 1,
        }
        url = '/api/productos_ventas/{}/'.format(producto_venta_test.id)
        response = self.client_401.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list(self):
        x_max = random.randint(1, 10)
        venta_test = G(Venta)
        for x in range(1, x_max + 1, 1):
            G(ProductoVenta, venta=venta_test)
        url = '/api/productos_ventas/'
        response = self.client.get(path=url, format='json')
        self.assertEquals(loads(dumps(response.data))['count'], x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        venta_test = G(Venta)
        for x in range(1, x_max + 1, 1):
            G(ProductoVenta, venta=venta_test)
        url = '/api/productos_ventas/'
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ProductoVentaListAPIViewTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_list(self):
        """
        Se genera un número aleatorio entre 1 y 10, se crea esa cantidad de Productos de venta pertenecientes a una
        venta, de igual forma se crea la misma cantidad de Productos de venta sin ser asignados a esa venta, el test
        debe arrojar la cantidad x_max de productos de venta pertenecientes a la empresa.
        """
        x_max = random.randint(1, 10)
        venta_test = G(Venta)
        for x in range(1, x_max + 1, 1):
            G(ProductoVenta, venta=venta_test)

        for x in range(1, x_max + 1, 1):
            G(Venta)
        url = '/api/produtos_venta_venta/{}/'.format(venta_test.id)
        response = self.client.get(path=url, format='json')
        self.assertEquals(len(loads(dumps(response.data))), x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        venta_test = G(Venta)
        for x in range(1, x_max + 1, 1):
            G(ProductoVenta, venta=venta_test)

        for x in range(1, x_max + 1, 1):
            G(Venta)
        url = '/api/produtos_venta_venta/{}/'.format(venta_test.id)
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)
