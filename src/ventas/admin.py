from django.contrib import admin

# Register your models here.
from ventas.models import Venta, ProductoVenta


class ProductoVentaInline(admin.StackedInline):
    model = ProductoVenta
    extra = 0
    raw_id_fields = ('producto',)
    exclude = ('creado_por', 'fecha_creacion', 'actualizado_por', 'ultima_actualizacion')


@admin.register(Venta)
class VentaAdmin(admin.ModelAdmin):
    list_display = ('cliente', 'get_empresa', 'total')
    list_display_links = ('cliente', 'get_empresa', 'total')
    search_fields = ('cliente__nombres', 'cliente__identificacion', 'cliente__correo')
    list_filter = ('cliente__empresa',)
    raw_id_fields = ('cliente',)
    readonly_fields = ('creado_por', 'fecha_creacion', 'actualizado_por', 'ultima_actualizacion')
    inlines = ProductoVentaInline,

    def get_empresa(self, obj):
        return obj.cliente.empresa.razon_social

    get_empresa.short_description = 'Empresa'
    get_empresa.admin_order_field = 'cliente__empresa__razon_social'
