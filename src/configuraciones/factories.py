import factory

from django.contrib.auth.models import User


class UserFactory(factory.Factory):

    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'user{0}'.format(n))
    email = factory.Sequence(lambda n: 'person{0}@example.com'.format(n))
    password = factory.PostGenerationMethodCall('set_password',
                                                'abcd1234')


class UserToLoginFactory(factory.Factory):

    class Meta:
        model = User

    username = "user"
    email = "user@user.com"
    password = factory.PostGenerationMethodCall('set_password',
                                                'abcd1234')


class UserWithCredentialsFactory(factory.Factory):
    class Meta:
        model = User

    username = "another"
    email = "another@user.com"
    password = factory.PostGenerationMethodCall('set_password',
                                                'abcd1234')
