# coding=utf-8
from django.contrib.auth.models import User
from django.db import models
from crum import get_current_user


class Auditoria(models.Model):

    creado_por = models.ForeignKey(User, on_delete=models.CASCADE,
                                   related_name='%(class)s_creado_por',
                                   null=True, blank=True,
                                   verbose_name='Usuario creación')
    fecha_creacion = models.DateTimeField(auto_now_add=True,
                                          verbose_name='fecha de creación',
                                          null=True)
    actualizado_por = models.ForeignKey(
        User, on_delete=models.CASCADE,
        related_name='%(class)s_modificado_por',
        null=True, blank=True,
        verbose_name='Usuario última actualización')

    ultima_actualizacion = models.DateTimeField(auto_now=True,
                                                verbose_name='última actualización',
                                                null=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if self.fecha_creacion is None:
            self.creado_por = get_current_user()
            self.actualizado_por = get_current_user()
        else:
            self.actualizado_por = get_current_user()
        super(Auditoria, self).save(*args, **kwargs)
