import json
from rest_framework.test import APIClient
from django_dynamic_fixture import G

from .factories import UserToLoginFactory


class DefaultAPITest:

    def __init__(self):
        self.create_authenticated_client()
        self.create_401_client()

    def create_authenticated_client(self):
        self.client = APIClient()

        self.user = UserToLoginFactory()
        self.user.save()

        response = self.client.post(
            '/api/token/',
            {'username': 'user', 'password': 'abcd1234'}, format='json')

        json_dict = json.loads(response.content)
        token = json_dict['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token {}'.format(token))

    @staticmethod
    def autenticate(username, password):
        client = APIClient()
        response = client.post(
            '/api/token/',
            {'username': username, 'password': password},
            format='json')

        json_dict = json.loads(response.content)
        print(json_dict)
        token = json_dict['token']
        client.credentials(HTTP_AUTHORIZATION='Token {}'.format(token))

        return client

    def create_401_client(self):
        self.client_401 = APIClient()
