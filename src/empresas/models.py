from django.db import models

# Create your models here.
from configuraciones.models import Auditoria


class Empresa(Auditoria):
    """
        Modelo para el almacenamiento de empresas
    """

    class Meta:
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'

    nombre = models.CharField(max_length=254, verbose_name='nombre')
    nit = models.CharField(max_length=20, verbose_name="NIT")
    razon_social = models.CharField(max_length=100, verbose_name="razón social")
    representante = models.CharField(max_length=254, verbose_name='representante legal')
    direccion = models.CharField(max_length=254, verbose_name='dirección')
    telefono = models.CharField(max_length=20, verbose_name='teléfono')
    correo = models.EmailField(max_length=254, verbose_name='correo electrónico')

    def __str__(self):
        return self.razon_social + ' | ' + self.nit


class Cliente(Auditoria):
    """
        Modelo para el almacenamiento de clientes de empresas
    """

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    nombres = models.CharField(max_length=254, verbose_name='nombres y apellidos')
    correo = models.EmailField(max_length=254, verbose_name='correo electrónico')
    identificacion = models.CharField(max_length=254, verbose_name='número de identificación')
    direccion = models.CharField(max_length=254, verbose_name='dirección')

    def nombre_empresa(self):
        if self.empresa:
            return u"%s" % (self.empresa.razon_social + ' | ' + self.empresa.nit)
        else:
            return u""

    nombre_empresa.short_description = 'Nombre empresa'
    nombre_empresa.allow_tags = False

    def __str__(self):
        return self.nombres
