from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError

from rest_framework import serializers

from .models import Empresa, Cliente


class EmpresaSerializer(serializers.ModelSerializer):
    """
        Serializador de datos para el modelo de Empresa
    """
    class Meta:
        model = Empresa
        fields = (
            'id',
            'nombre',
            'nit',
            'razon_social',
            'representante',
            'direccion',
            'telefono',
            'correo'
        )


class ClienteSerializer(serializers.ModelSerializer):
    """
        Serializador de datos para el modelo de Cliente
    """
    class Meta:
        model = Cliente
        fields = (
            'id',
            'empresa',
            'nombre_empresa',
            'nombres',
            'correo',
            'identificacion',
            'direccion',
        )
        read_only_fields = ('nombre_empresa',)
