from django.urls import path, include
from django.conf.urls import url

from rest_framework.routers import SimpleRouter

from .views import EmpresaViewSet, ClienteViewSet, ClienteListAPIView

router = SimpleRouter()

router.register(r'empresas', EmpresaViewSet)
router.register(r'clientes', ClienteViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('clientes_empresa/<int:empresa>/', ClienteListAPIView.as_view(), name='clientes_list'),

]
