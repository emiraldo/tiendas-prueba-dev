from rest_framework import viewsets
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from .models import Empresa, Cliente
from .serializers import EmpresaSerializer, ClienteSerializer


class EmpresaViewSet(viewsets.ModelViewSet):
    """
       retrieve:
       Retorna información de una empresa dado su ID.

       list:
       Retorna el listado de empresas existentes.

       create:
       Servicio para la creación de una empresa.

       update:
       Servicio para actualizar una empresa (idempotente).

       partial_update:
       Servicio para actualizar una empresa.

       delete:
       Servicio para eliminar una empresa.
    """
    serializer_class = EmpresaSerializer
    queryset = Empresa.objects.all()
    http_method_names = ('post', 'put', 'get', 'patch', 'delete')


class ClienteViewSet(viewsets.ModelViewSet):
    """
       retrieve:
       Retorna información de un cliente dado su ID.

       list:
       Retorna el listado de clientes existentes.

       create:
       Servicio para la creación de un cliente.

       update:
       Servicio para actualizar un cliente (idempotente).

       partial_update:
       Servicio para actualizar un cliente.

       delete:
       Servicio para eliminar un cliente.
    """
    serializer_class = ClienteSerializer
    queryset = Cliente.objects.all()
    http_method_names = ('post', 'put', 'get', 'patch', 'delete')


class ClienteListAPIView(ListAPIView):
    """
    Retorna el listado de clientes existentes a partir de una empresa.
    """
    queryset = []
    serializer_class = ClienteSerializer
    lookup_url_kwarg = 'empresa'

    def get(self, request, *args, **kwargs):

        empresa = self.kwargs.get(self.lookup_url_kwarg)
        queryset = Cliente.objects.filter(
            empresa__pk=empresa
        )
        clientes = []
        for cliente in queryset:
            clientes.append({
                'id': cliente.pk,
                'empresa': cliente.empresa.pk,
                'nombre_empresa': cliente.nombre_empresa(),
                'nombres': cliente.nombres,
                'correo': cliente.correo,
                'identificacion': cliente.identificacion,
                'direccion': cliente.direccion,
            })

        return Response(clientes)
