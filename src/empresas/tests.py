import random
from json import dumps, loads

from django.contrib.auth.models import User
from django.test import TestCase

from django_dynamic_fixture import G
from rest_framework import status

from configuraciones.utils import DefaultAPITest
from empresas.models import Empresa, Cliente


class EmpresaViewSetTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_destroy(self):
        empresa_test = G(Empresa)

        url = '/api/empresas/{}/'.format(empresa_test.id)
        response = self.client.delete(url)

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_destroy_unauthorized(self):
        empresa_test = G(Empresa)
        url = '/api/empresas/{}/'.format(empresa_test.id)
        response = self.client_401.delete(url)

        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create(self):
        data = {
            'nombre': 'Empresa de prueba',
            'nit': '123123123',
            'razon_social': 'Empresa de prueba RS',
            'representante': 'Persona representante',
            'direccion': 'Calle falsa 123',
            'telefono': '11223344',
            'correo': 'micorreo@correo.com'
        }
        url = '/api/empresas/'
        response = self.client.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_create_unauthorized(self):
        data = {
            'nombre': 'Empresa de prueba',
            'nit': '123123123',
            'razon_social': 'Empresa de prueba RS',
            'representante': 'Persona representante',
            'direccion': 'Calle falsa 123',
            'telefono': '11223344',
            'correo': 'micorreo@correo.com'
        }
        url = '/api/empresas/'
        response = self.client_401.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_put(self):
        empresa_test = G(Empresa)
        data = {
            'nombre': 'Empresa de prueba',
            'nit': '123123123',
            'razon_social': 'Empresa de prueba RS',
            'representante': 'Persona representante',
            'direccion': 'Calle falsa 123',
            'telefono': '11223344',
            'correo': 'micorreo@correo.com'
        }
        url = '/api/empresas/{}/'.format(empresa_test.id)
        response = self.client.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_put_unauthorized(self):
        empresa_test = G(Empresa)
        data = {
            'nombre': 'Empresa de prueba',
            'nit': '123123123',
            'razon_social': 'Empresa de prueba RS',
            'representante': 'Persona representante',
            'direccion': 'Calle falsa 123',
            'telefono': '11223344',
            'correo': 'micorreo@correo.com'
        }
        url = '/api/empresas/{}/'.format(empresa_test.id)
        response = self.client_401.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_patch(self):
        empresa_test = G(Empresa)
        data = {
            'nombre': 'Empresa de prueba',
        }
        url = '/api/empresas/{}/'.format(empresa_test.id)
        response = self.client.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_patch_unauthorized(self):
        empresa_test = G(Empresa)
        data = {
            'nombre': 'Empresa de prueba',
        }
        url = '/api/empresas/{}/'.format(empresa_test.id)
        response = self.client_401.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list(self):
        x_max = random.randint(1, 10)
        for x in range(1, x_max + 1, 1):
            G(Empresa)
        url = '/api/empresas/'
        response = self.client.get(path=url, format='json')
        self.assertEquals(loads(dumps(response.data))['count'], x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        for x in range(1, x_max + 1, 1):
            G(Empresa)
        url = '/api/empresas/'
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ClienteViewSetTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_destroy(self):
        empresa_test = G(Empresa)
        cliente_test = G(Cliente, empresa__pk=empresa_test.pk)

        url = '/api/clientes/{}/'.format(cliente_test.id)
        response = self.client.delete(url)

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_destroy_unauthorized(self):
        empresa_test = G(Empresa)
        cliente_test = G(Cliente, empresa__pk=empresa_test.pk)
        url = '/api/clientes/{}/'.format(cliente_test.id)
        response = self.client_401.delete(url)

        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create(self):
        empresa_test = G(Empresa)
        data = {
            'empresa': empresa_test.id,
            'nombres': 'Cliente Prueba',
            'correo': 'cliente@prueba.com',
            'identificacion': '1122334455',
            'direccion': 'Calle falsa 123',
        }
        url = '/api/clientes/'
        response = self.client.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_create_unauthorized(self):
        empresa_test = G(Empresa)
        data = {
            'empresa': empresa_test.id,
            'nombres': 'Cliente Prueba',
            'correo': 'cliente@prueba.com',
            'identificacion': '1122334455',
            'direccion': 'Calle falsa 123',
        }
        url = '/api/clientes/'
        response = self.client_401.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_put(self):
        empresa_test = G(Empresa)
        cliente_test = G(Cliente)
        data = {
            'empresa': empresa_test.pk,
            'nombres': 'Cliente Prueba',
            'correo': 'cliente@prueba.com',
            'identificacion': '1122334455',
            'direccion': 'Calle falsa 123',
        }
        url = '/api/clientes/{}/'.format(cliente_test.id)
        response = self.client.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_put_unauthorized(self):
        empresa_test = G(Empresa)
        cliente_test = G(Cliente)
        data = {
            'empresa': empresa_test.pk,
            'nombres': 'Cliente Prueba',
            'correo': 'cliente@prueba.com',
            'identificacion': '1122334455',
            'direccion': 'Calle falsa 123',
        }
        url = '/api/clientes/{}/'.format(cliente_test.id)
        response = self.client_401.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_patch(self):
        empresa_test = G(Empresa)
        cliente_test = G(Cliente, empresa__pk=empresa_test.pk)
        data = {
            'nombres': 'Cliente Prueba',
        }
        url = '/api/clientes/{}/'.format(cliente_test.id)
        response = self.client.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_patch_unauthorized(self):
        empresa_test = G(Empresa)
        cliente_test = G(Cliente, empresa__pk=empresa_test.pk)
        data = {
            'nombres': 'Cliente Prueba',
        }
        url = '/api/clientes/{}/'.format(cliente_test.id)
        response = self.client_401.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list(self):
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        for x in range(1, x_max + 1, 1):
            G(Cliente, empresa__pk=empresa_test.pk)
        url = '/api/clientes/'
        response = self.client.get(path=url, format='json')
        self.assertEquals(loads(dumps(response.data))['count'], x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        for x in range(1, x_max + 1, 1):
            G(Cliente, empresa__pk=empresa_test.pk)
        url = '/api/clientes/'
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ClienteListAPIViewTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_list(self):
        """
        Se genera un número aleatorio entre 1 y 10, se crea esa cantidad de Clientes pertenecientes a una empresa,
        de igual forma se crea la misma cantidad de Clientes sin ser asignados a esa empresa, el test debe arrojar
        la cantidad x_max de Clientes pertenecientes a la empresa.
        """
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        for x in range(1, x_max + 1, 1):
            G(Cliente, empresa=empresa_test)

        for x in range(1, x_max + 1, 1):
            G(Cliente)
        url = '/api/clientes_empresa/{}/'.format(empresa_test.id)
        response = self.client.get(path=url, format='json')
        self.assertEquals(len(loads(dumps(response.data))), x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        for x in range(1, x_max + 1, 1):
            G(Cliente, empresa=empresa_test)

        for x in range(1, x_max + 1, 1):
            G(Cliente)
        url = '/api/clientes_empresa/{}/'.format(empresa_test.id)
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)
