from django.contrib import admin

# Register your models here.
from .models import Empresa, Cliente


@admin.register(Empresa)
class EmpresaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'razon_social', 'representante')
    list_display_links = ('nombre', 'razon_social', 'representante')
    search_fields = ('nombre', 'razon_social', 'representante')
    readonly_fields = ('creado_por', 'fecha_creacion', 'actualizado_por', 'ultima_actualizacion')


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = ('nombres', 'empresa', 'correo')
    list_display_links = ('nombres', 'empresa', 'correo')
    search_fields = ('nombres', 'correo', 'identificacion')
    raw_id_fields = ('empresa',)
    list_filter = ('empresa',)
    readonly_fields = ('creado_por', 'fecha_creacion', 'actualizado_por', 'ultima_actualizacion')
