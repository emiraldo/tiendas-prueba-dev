from django.contrib import admin

# Register your models here.
from productos.models import Producto


@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'empresa', 'cantidad', 'valor_venta')
    list_display_links = ('nombre', 'empresa', 'cantidad', 'valor_venta')
    search_fields = ('nombre',)
    list_filter = ('empresa',)
    raw_id_fields = ('empresa',)
    readonly_fields = ('creado_por', 'fecha_creacion', 'actualizado_por', 'ultima_actualizacion')
