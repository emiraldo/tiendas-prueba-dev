from django.db import models

# Create your models here.
from configuraciones.models import Auditoria
from empresas.models import Empresa


class Producto(Auditoria):
    """
        Modelo para almacenamiento de productos de empresas
    """

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=254, verbose_name='nombre')
    descripcion = models.TextField(verbose_name='descripción')
    valor_venta = models.PositiveIntegerField(default=0)
    cantidad = models.PositiveIntegerField(default=0)

    def nombre_empresa(self):
        if self.empresa:
            return u"%s" % (self.empresa.razon_social + ' | ' + self.empresa.nit)
        else:
            return u""

    nombre_empresa.short_description = 'Nombre empresa'
    nombre_empresa.allow_tags = False

    def __str__(self):
        return self.nombre + ' | ' + str(self.empresa)
