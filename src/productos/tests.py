import random
from json import dumps, loads

from django.contrib.auth.models import User
from django.test import TestCase

from django_dynamic_fixture import G
from rest_framework import status

from configuraciones.utils import DefaultAPITest
from empresas.models import Empresa
from productos.models import Producto


class ProductoListAPIViewTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_list(self):
        """
        Se genera un número aleatorio entre 1 y 10, se crea esa cantidad de productos pertenecientes a una empresa,
        de igual forma se crea la misma cantidad de productos sin ser asignados a esa empresa, el test debe arrojar
        la cantidad x_max de productos pertenecientes a la empresa.
        """
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        for x in range(1, x_max + 1, 1):
            G(Producto, empresa=empresa_test)

        for x in range(1, x_max + 1, 1):
            G(Producto)
        url = '/api/productos_empresa/{}/'.format(empresa_test.id)
        response = self.client.get(path=url, format='json')
        self.assertEquals(len(loads(dumps(response.data))), x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        for x in range(1, x_max + 1, 1):
            G(Producto, empresa__id=empresa_test.id)

        for x in range(1, x_max + 1, 1):
            G(Producto)
        url = '/api/productos_empresa/{}/'.format(empresa_test.id)
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ProductoViewSetTest(TestCase, DefaultAPITest):

    def setUp(self):
        DefaultAPITest.__init__(self)

        self.usuario = G(User)
        self.client.login()

    def test_destroy(self):
        producto_test = G(Producto)

        url = '/api/productos/{}/'.format(producto_test.id)
        response = self.client.delete(url)

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_destroy_unauthorized(self):
        producto_test = G(Producto)
        url = '/api/productos/{}/'.format(producto_test.id)
        response = self.client_401.delete(url)

        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create(self):
        empresa_test = G(Empresa)
        data = {
            'empresa': empresa_test.id,
            'nombre': 'Producto de prueba',
            'descripcion': 'Descripción de prueba',
            'valor_venta': 1000,
            'cantidad': 1,
        }
        url = '/api/productos/'
        response = self.client.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_create_unauthorized(self):
        empresa_test = G(Empresa)
        data = {
            'empresa': empresa_test.id,
            'nombre': 'Producto de prueba',
            'descripcion': 'Descripción de prueba',
            'valor_venta': 1000,
            'cantidad': 1,
        }
        url = '/api/productos/'
        response = self.client_401.post(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_put(self):
        empresa_test = G(Empresa)
        producto_test = G(Producto, empresa__id=empresa_test.id)
        data = {
            'empresa': empresa_test.id,
            'nombre': 'Producto de prueba',
            'descripcion': 'Descripción de prueba',
            'valor_venta': 1000,
            'cantidad': 1,
        }
        url = '/api/productos/{}/'.format(producto_test.id)
        response = self.client.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_put_unauthorized(self):
        empresa_test = G(Empresa)
        producto_test = G(Producto, empresa=empresa_test)
        data = {
            'empresa': empresa_test.id,
            'nombre': 'Producto de prueba',
            'descripcion': 'Descripción de prueba',
            'valor_venta': 1000,
            'cantidad': 1,
        }
        url = '/api/productos/{}/'.format(producto_test.id)
        response = self.client_401.put(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_patch(self):
        empresa_test = G(Empresa)
        producto_test = G(Producto, empresa=empresa_test)
        data = {
            'nombre': 'Producto de prueba',
        }
        url = '/api/productos/{}/'.format(producto_test.id)
        response = self.client.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_update_patch_unauthorized(self):
        empresa_test = G(Empresa)
        producto_test = G(Producto, empresa=empresa_test)
        data = {
            'nombre': 'Producto de prueba',
        }
        url = '/api/productos/{}/'.format(producto_test.id)
        response = self.client_401.patch(path=url, data=data, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list(self):
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        for x in range(1, x_max + 1, 1):
            G(Producto, empresa=empresa_test)
        url = '/api/productos/'
        response = self.client.get(path=url, format='json')
        self.assertEquals(loads(dumps(response.data))['count'], x_max)

    def test_list_unauthorized(self):
        x_max = random.randint(1, 10)
        empresa_test = G(Empresa)
        for x in range(1, x_max + 1, 1):
            G(Producto, empresa=empresa_test)
        url = '/api/productos/'
        response = self.client_401.get(path=url, format='json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)
