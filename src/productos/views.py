from rest_framework import viewsets
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from .models import Producto
from .serializers import ProductoSerializer


class ProductoViewSet(viewsets.ModelViewSet):
    """
       retrieve:
       Retorna información de un producto dado su ID.

       list:
       Retorna el listado de productos existentes.

       create:
       Servicio para la creación de un producto.

       update:
       Servicio para actualizar un producto (idempotente).

       partial_update:
       Servicio para actualizar un producto.

       delete:
       Servicio para eliminar un producto.
    """
    serializer_class = ProductoSerializer
    queryset = Producto.objects.all()
    http_method_names = ('post', 'put', 'get', 'patch', 'delete')


class ProductoListAPIView(ListAPIView):
    """
    Retorna el listado de productos existentes a partir de una empresa.
    """
    queryset = []
    serializer_class = ProductoSerializer
    lookup_url_kwarg = 'empresa'

    def get(self, request, *args, **kwargs):

        empresa = self.kwargs.get(self.lookup_url_kwarg)
        queryset = Producto.objects.filter(
            empresa__pk=empresa
        )
        productos = []
        for producto in queryset:
            productos.append({
                'id': producto.pk,
                'empresa': producto.empresa.pk,
                'nombre_empresa': producto.nombre_empresa(),
                'nombre': producto.nombre,
                'descripcion': producto.descripcion,
                'valor_venta': producto.valor_venta,
                'cantidad': producto.cantidad,
            })

        return Response(productos)
