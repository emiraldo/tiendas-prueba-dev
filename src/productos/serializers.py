from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError

from rest_framework import serializers

from .models import Producto


class ProductoSerializer(serializers.ModelSerializer):
    """
        Serializador de datos para el modelo de Producto
    """
    class Meta:
        model = Producto
        fields = (
            'id',
            'empresa',
            'nombre_empresa',
            'nombre',
            'descripcion',
            'valor_venta',
            'cantidad',
        )
        read_only_fields = ('nombre_empresa',)
