from django.urls import path, include
from django.conf.urls import url

from rest_framework.routers import SimpleRouter

from .views import ProductoViewSet, ProductoListAPIView

router = SimpleRouter()

router.register(r'productos', ProductoViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('productos_empresa/<int:empresa>/', ProductoListAPIView.as_view(), name='productos_list'),
]
