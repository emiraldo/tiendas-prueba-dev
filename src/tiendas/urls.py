"""tiendas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.conf.urls.static import static
from rest_framework.authtoken.views import obtain_auth_token

from configuraciones.views import Logout
from tiendas.settings import base as settings

from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Tiendas API')


urlpatterns = \
    [
        path('jet/', include('jet.urls', 'jet')),
        path('admin/', admin.site.urls),
        path('api/', schema_view),
        path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
        path('api/token/', obtain_auth_token, name='get_auth_token'),
        path('api/token-logout/', Logout.as_view(), name='auth_token_logout'),
        path('api/', include('empresas.urls')),
        path('api/', include('productos.urls')),
        path('api/', include('ventas.urls')),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = u'Administración sistema de Tiendas'
admin.site.site_title = u"Sitio de administración"

