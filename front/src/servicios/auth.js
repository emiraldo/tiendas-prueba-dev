import axios from 'axios'
import {host} from '../globals'

export default {
    login: async function (form){
        return await axios.post(host + '/token/', form);
    },
    logout: async function (){
        return await axios.get(host + '/token-logout/', {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    }
    
} 