import axios from 'axios'
import {host} from '../globals'
import { async } from 'q';

export default {
    list: async function (id){
        return await axios.get(host + '/ventas_empresa/' + id + '/',{
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    detail: async function (id){
        return await axios.get(host + '/ventas/' + id + '/',{
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    create: async function(form){
        return await axios.post(host + '/ventas/', form, {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    update: async function(id, form){
        return await axios.put(host + '/ventas/' + id + '/', form, {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    delete: async function(id){
        return await axios.delete(host + '/ventas/' + id, {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    }
    
} 