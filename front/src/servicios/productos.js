import axios from 'axios'
import {host} from '../globals'
import { async } from 'q';

export default {
    list: async function (id){
        return await axios.get(host + '/productos_empresa/' + id + '/',{
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    list_productos_venta: async function (id){
        return await axios.get(host + '/produtos_venta_venta/' + id + '/',{
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    detail: async function (id){
        return await axios.get(host + '/productos/' + id + '/',{
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    create: async function(form){
        return await axios.post(host + '/productos/', form, {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    create_productos_venta: async function(form){
        return await axios.post(host + '/productos_ventas/', form, {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    update: async function(id, form){
        return await axios.put(host + '/productos/' + id + '/', form, {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    delete: async function(id){
        return await axios.delete(host + '/productos/' + id, {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    },
    delete_productos_venta: async function(id){
        return await axios.delete(host + '/productos_ventas/' + id, {
            headers: {
                'Authorization': 'Token ' + localStorage.getItem('token'),
            }
        });
    }
} 