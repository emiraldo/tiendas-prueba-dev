
import VueRouter from 'vue-router'

import Login from './pages/login'

import EmpresasList from './pages/empresas/list'
import EmpresasDetail from './pages/empresas/detail'
import EmpresasCreate from './pages/empresas/create'
import EmpresasUpdate from './pages/empresas/update'

import ClientesCreate from './pages/clientes/create'
import ClientesUpdate from './pages/clientes/update'
import ClientesDetail from './pages/clientes/detail'

import ProductosCreate from './pages/productos/create'
import ProductosUpdate from './pages/productos/update'

import VentasCreate from './pages/ventas/create'
import VentasDetail from './pages/ventas/detail'

const router = new VueRouter({
    routes: [
        {path:'/login', component: Login, name:'login'},
        {path:'/empresas-list', component: EmpresasList, name:'empresas_list'},
        {path:'/empresas-detail/:id', component: EmpresasDetail, name:'empresas_detail'},
        {path:'/empresas-create', component: EmpresasCreate, name:'empresas_create'},
        {path:'/empresas-update/:id', component: EmpresasUpdate, name:'empresas_update'},

        {path:'/clientes-create/:id_empresa', component: ClientesCreate, name:'clientes_create'},
        {path:'/clientes-update/:id', component: ClientesUpdate, name:'clientes_update'},
        {path:'/clientes-detail/:id', component: ClientesDetail, name:'clientes_detail'},

        {path:'/productos-create/:id_empresa', component: ProductosCreate, name:'productos_create'},
        {path:'/productos-update/:id', component: ProductosUpdate, name:'productos_update'},

        {path:'/ventas-create/:id_empresa', component: VentasCreate, name:'ventas_create'},
        {path:'/ventas-detail/:id', component: VentasDetail, name:'ventas_detail'},
    ]
})

export default router;